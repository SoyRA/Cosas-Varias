# Ramas
- Depende lo que vaya a aportar, se hará bajo una [rama especifica](https://gitlab.com/SoyRA/Cosas-Varias/branches/all).
  

# Wiki
- Puede que haya escrito algo en la [Wiki](https://gitlab.com/SoyRA/Cosas-Varias/wikis), como una guía para preparar un Café Batido. ;)


# Créditos
- Siempre intentaré dar créditos a los que me hayan dado una idea, una línea de código, etc. Ya que me gustaría que me den créditos a mi, sería justo que yo haga lo mismo. :P
  - Si en algo no he dado créditos, y sabes quien es el autor original para poder mencionarlo, hazmelo saber escribiendome a uno de mis [Medios de Contacto](https://gitlab.com/snippets/1795881#c%C3%B3mo-te-puedo-contactar)

# Licencia
- Todo lo que este en este Repositorio, esta bajo [Licencia MIT](https://gitlab.com/SoyRA/Cosas-Varias/blob/master/LICENSE).
  - Básicamente, podes hacer todo lo que quieras...pero deberás darme créditos.
